Seisho Go

# Session 1
I am taking this course because it is required. 
I have learned C++ when I took CIS121, but I could not understand well. 
Recently, my friend suggested Python to me, and it seems much easier to use.
So I decided to select Python.
I do not know what can I do with Python in my real life yet, but I am ready to explore it.

# Session 2
I learned how to change a program and how to show the changes in an IDE.
I chose to use Cloud9 for IDE.
I guess when I start working I will have to share my code with coworkers,
so IDEs can be very useful to share codes and changes.

# Session 3
